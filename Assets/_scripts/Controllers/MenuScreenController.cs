﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuScreenController : MonoBehaviour
{
    public Text gamesPlayedText;
    public Text onlinePlayersText;
    public Text raitingValueText;
    public Text pointsValueText;

    public GameObject userProgressViewPrefab;
    public GameObject leaderboardScreen;
    public GameObject leaderboardContent;

    public GameObject settingsPanel;
    public GameObject settingsContent;

    DataController dataController;

    private void Start()
    {
        dataController = FindObjectOfType<DataController>();

        UpdateDashboardView();
        Messenger.MarkAsPermanent("OnDashboardDataUpdated");
        Messenger.AddListener("OnDashboardDataUpdated", UpdateDashboardView);

        NotificationData newNotification = dataController.GetNextNotification();
        if(newNotification != null)
            onNewNotification(newNotification);

        dataController.OnMenuScreenLoaded();
    }
    private void OnDestroy()
    {
        Messenger.RemoveListener("OnDashboardDataUpdated", UpdateDashboardView);
        dataController.OnMenuScreenLeft();
    }

    /* SETTINGS METHODS */
    public void OpenSettings()
    {
        settingsPanel.SetActive(true);

        UserData currentUser = dataController.GetUserData();

        UserProfileInfoView userProfileInfoView = settingsPanel.transform.FindDeepChild("UserProfileInfoView").GetComponent<UserProfileInfoView>();
        userProfileInfoView.LoadInfo(currentUser.ProfilePhoto, currentUser.Name, currentUser.Surname, 0);
    }
    public void OnLogOutClicked()
    {
        dataController.OnLoggedOut();
        SceneManager.LoadScene("Auth");
    }

    /* UI HANDLERS */
    public void OnQuitClicked()
    {
        Application.Quit();
    }

    /* LEADERBOARD METHODS */
    public void OpenLeaderboard()
    {
        UpdateLeaderboardView();
        leaderboardScreen.SetActive(true);
    }
    private void UpdateLeaderboardView()
    {
        ClearLeaders();

        leaderboardScreen.transform.FindDeepChild("LoadingText").gameObject.SetActive(true);
        dataController.DownloadTop10((leaderboard) =>
        {
            leaderboardScreen.transform.FindDeepChild("LoadingText").gameObject.SetActive(false);
            for (int i = 0; i < leaderboard.AllUsers.Length; i++)
            {
                Debug.Log("i = " + i + "Instatiated");
                GameObject userProgressView = Instantiate(userProgressViewPrefab, leaderboardContent.transform);
                userProgressView.GetComponent<UserLeaderboardView>().loadViewData(i + 1, leaderboard.AllUsers[i]);
            }
        });
    }
    private void ClearLeaders()
    {
        foreach (Transform leaderItem in leaderboardContent.transform)
        {
            Destroy(leaderItem.gameObject);
        }
    }


    /* MULTIPLAYER METHODS */
    public void OnPlayWithPseudoRandom()
    {
        SceneManager.LoadScene("SessionScreen");
    }
    public void OnPlayWithFriend()
    {
        //edit. add realization

        Debug.LogWarning("OnPlatWithFriend() clicked. Empty. No Realization");
    }

    /* UPDATE VIEW METHODS */
    private void UpdateDashboardView()
    {
        //load online players info
        int onlinePlayers = dataController.GetOnlinePlayersCount() - 1; //exclusive user
        onlinePlayersText.text = onlinePlayers.ToString();

        StatisticsData statistics = dataController.GetUserStatisticsData();
        //load game stat info
        gamesPlayedText.text = statistics.GamesPlayed.ToString();
        
        //load score data
        UserProgressData userProgress = dataController.GetUserProgressData();
        if (userProgress != null)
        {
            //edit. add rating data
            pointsValueText.text = userProgress.Points.ToString();
        }
        else
        {
            Debug.Log("User progress is null");
        }
    }

    /* NOTIFICATION */
    NotificationData activeNotification;
    public void onNewNotification(NotificationData notification)
    {
        activeNotification = notification;

        //edit. add realization
    }
}
