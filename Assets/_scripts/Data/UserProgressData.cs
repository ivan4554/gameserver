﻿using FullSerializer;
public class UserProgressData
{
    [fsProperty] private string name;
    [fsProperty] private string surname;
    [fsProperty] private int points;

    public UserProgressData(string name, string surname, int score)
    {
        this.name = name;
        this.surname = surname;
        this.points = score;
    }

    public string Name { get => name; set => name = value; }
    public string Surname { get => surname; set => surname = value; }
    public int Points { get => points; set => points = value; }
    

    public override string ToString()
    {
        return $"[name - {name}, points - {points}]";
    }
}
